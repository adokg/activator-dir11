import {Authorization} from './authorization';

export class Gui extends Object {
	constructor(
		form,
		activationNumberName,
		randomAName,
		randomBName,
		authorizationCodeName
	) {
		super();

		this.form = form;
		this.activationNumberName = activationNumberName;
		this.randomAName = randomAName;
		this.randomBName = randomBName;
		this.authorizationCodeName = authorizationCodeName;
		this.submitCallback = null;
	}

	getFormElements(name) {
		return this.form.querySelectorAll(`[name="${name}"]`);
	}

	getFormValue(name) {
		const elements = this.getFormElements(name);
		for (const element of elements) {
			switch (element.type.toLowerCase()) {
				case 'checkbox': 
				case 'radio': {
					if (element.checked) {
						return element.value;
					}
					break;
				}
				default: {
					return element.value;
				}
			}
		}
		return null;
	}

	setFormValue(name, value) {
		const elements = this.getFormElements(name);
		for (const element of elements) {
			element.value = value;
		}
	}

	init() {
		this.submitCallback = e => {
			e.preventDefault();
			const activationNumber = this.getFormValue(this.activationNumberName);
			const randomA = +this.getFormValue(this.randomAName) || null;
			const randomB = +this.getFormValue(this.randomBName) || null;

			const auth = new Authorization();
			try {
				const response = auth.code(activationNumber, randomA, randomB);
				this.setFormValue(this.authorizationCodeName, response);
			}
			catch (err) {
				this.setFormValue(this.authorizationCodeName, '' + err);
				throw err;
			}
		};

		this.form.addEventListener('submit', this.submitCallback);
	}

	destroy() {
		this.form.removeEventListener('submit', this.submitCallback);
	}
}
