// A modified SHA1 hasher that produces non-standard hashes.
// Based on:
// http://mattmahoney.net/dc/sha1.c
// http://mattmahoney.net/dc/sha1.h

function uint32(i) {
	return (i & 0xFFFFFFFF) >>> 0;
}

function circularShift(bits, word) {
	return uint32((word << bits) | (word >>> (32 - bits)));
}

export class AdoSha1 extends Object {
	constructor() {
		super();

		this.intermediateHash = new Uint32Array(5);

		this.lengthLow = 0;
		this.lengthHigh = 0;

		this.messageBlockIndex = 0;
		this.messageBlock = new Uint8Array(64);

		this.computed = false;
		this.corrupted = false;

		this.reset();
	}

	reset() {
		this.lengthLow = 0;
		this.lengthHigh = 0;
		this.messageBlockIndex = 0;

		this.intermediateHash[0] = 0x67452301;
		this.intermediateHash[1] = 0xEFCDAB89;
		this.intermediateHash[2] = 0x98BADCFE;
		this.intermediateHash[3] = 0x10325476;
		this.intermediateHash[4] = 0xC3D2E1F0;

		this.computed = false;
		this.corrupted = false;
	}

	result() {
		const digest = new Uint8Array(20);

		if (this.corrupted) {
			throw new Error('Corrupt state');
		}

		if (!this.computed) {
			this.padMessage();
			for (let i = 0; i < 64; i++) {
				this.messageBlock[i] = 0;
			}
			this.lengthLow = 0; 
			this.lengthHigh = 0; 
			this.computed = true; 
		}

		for (let i = 0; i < 20; i++) {
			digest[i] = this.intermediateHash[i >> 2] >> (
				8 * (3 - (i & 0x03))
			);
		}

		return digest;
	}

	input(data) {
		const len = data.length;

		if (!len) {
			return;
		}

		if (this.computed) {
			this.corrupted = true;
		}

		if (this.corrupted) {
			throw new Error('Corrupt state');
		}

		for (let i = 0; i < len; i++) {
			this.messageBlock[this.messageBlockIndex] = data[i] & 0xFF;
			this.messageBlockIndex = uint32(this.messageBlockIndex + 1);

			this.lengthLow = uint32(this.lengthLow + 8);
			if (!this.lengthLow) {
				this.lengthHigh = uint32(this.lengthHigh + 1);
				if (!this.lengthHigh) {
					this.corrupted = true;
					throw new Error('Corrupt state');
				}
			}

			if (this.messageBlockIndex == 64) {
				this.processMessageBlock();
			}
		}
	}

	processMessageBlock() {
		const K0 = 0x5A827999;
		const K1 = 0x6ED9EBA1;
		const K2 = 0x8F1BBCDC;
		const K3 = 0xCA62C1D6;

		const W = new Uint32Array(80);

		for (let t = 0; t < 16; t++) {
			// ORIGINAL:
			// W[t] = this.messageBlock[t * 4] << 24;
			// W[t] |= this.messageBlock[t * 4 + 1] << 16;
			// W[t] |= this.messageBlock[t * 4 + 2] << 8;
			// W[t] |= this.messageBlock[t * 4 + 3];
			// PATCH:
			W[t] = this.messageBlock[t * 4];
			W[t] |= this.messageBlock[t * 4 + 1] << 8;
			W[t] |= this.messageBlock[t * 4 + 2] << 16;
			W[t] |= this.messageBlock[t * 4 + 3] << 24;
			// END
		}

		for (let t = 16; t < 80; t++) {
			W[t] = circularShift(
				1,
				uint32(W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16])
			);
		}

		let A = this.intermediateHash[0];
		let B = this.intermediateHash[1];
		let C = this.intermediateHash[2];
		let D = this.intermediateHash[3];
		let E = this.intermediateHash[4];

		for (let t = 0; t < 20; t++) {
			const temp = uint32(
				circularShift(5, A) +
				uint32((B & C) | ((~B) & D)) +
				E +
				W[t] +
				K0
			);
			E = D;
			D = C;
			C = circularShift(30, B);
			B = A;
			A = temp;
		}

		for (let t = 20; t < 40; t++) {
			const temp = uint32(
				circularShift(5, A) +
				uint32(B ^ C ^ D) +
				E +
				W[t] +
				K1
			);
			E = D;
			D = C;
			C = circularShift(30, B);
			B = A;
			A = temp;
		}

		for (let t = 40; t < 60; t++) {
			const temp = uint32(
				circularShift(5, A) +
				uint32((B & C) | (B & D) | (C & D)) +
				E +
				W[t] +
				K2
			);
			E = D;
			D = C;
			C = circularShift(30, B);
			B = A;
			A = temp;
		}

		for (let t = 60; t < 80; t++) {
			const temp = uint32(
				circularShift(5, A) +
				uint32(B ^ C ^ D) +
				E +
				W[t] +
				K3
			);
			E = D;
			D = C;
			C = circularShift(30, B);
			B = A;
			A = temp;
		}

		this.intermediateHash[0] += A;
		this.intermediateHash[1] += B;
		this.intermediateHash[2] += C;
		this.intermediateHash[3] += D;

		// ORIGINAL:
		// this.intermediateHash[4] += E;
		// PATCH:
		this.intermediateHash[4] = this.intermediateHash[3];
		// END

		this.messageBlockIndex = 0;
	}

	padMessage() {
		if (this.messageBlockIndex > 55) {
			this.messageBlock[this.messageBlockIndex] = 0x80;
			this.messageBlockIndex = uint32(this.messageBlockIndex + 1);
			while (this.messageBlockIndex < 64) {
				this.messageBlock[this.messageBlockIndex] = 0;
				this.messageBlockIndex = uint32(this.messageBlockIndex + 1);
			}

			this.processMessageBlock();
		}
		else {
			this.messageBlock[this.messageBlockIndex] = 0x80;
			this.messageBlockIndex = uint32(this.messageBlockIndex + 1);
		}

		while (this.messageBlockIndex < 56) {
			this.messageBlock[this.messageBlockIndex] = 0;
			this.messageBlockIndex = uint32(this.messageBlockIndex + 1);
		}

		// ORIGINAL:
		// this.messageBlock[56] = this.lengthHigh >> 24;
		// this.messageBlock[57] = this.lengthHigh >> 16;
		// this.messageBlock[58] = this.lengthHigh >> 8;
		// this.messageBlock[59] = this.lengthHigh;
		// this.messageBlock[60] = this.lengthLow >> 24;
		// this.messageBlock[61] = this.lengthLow >> 16;
		// this.messageBlock[62] = this.lengthLow >> 8;
		// this.messageBlock[63] = this.lengthLow;
		// PATCH:
		this.messageBlock[56] = this.lengthHigh;
		this.messageBlock[57] = this.lengthHigh >> 8;
		this.messageBlock[58] = this.lengthHigh >> 16;
		this.messageBlock[59] = this.lengthHigh >> 24;
		this.messageBlock[60] = this.lengthLow;
		this.messageBlock[61] = this.lengthLow >> 8;
		this.messageBlock[62] = this.lengthLow >> 16;
		this.messageBlock[63] = this.lengthLow >> 24;
		// END

		this.processMessageBlock();
	}
}
