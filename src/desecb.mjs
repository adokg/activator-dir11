import crypto from 'crypto';

import {
	bufferFrom,
	uint8ArraySlice
} from './util';

export class DesEcb extends Object {
	constructor(key) {
		super();

		this.key = key;
	}

	encrypt(buff) {
		return this._process(
			crypto.createCipheriv('des-ecb', this.key, ''),
			buff
		);
	}

	decrypt(buff) {
		return this._process(
			crypto.createDecipheriv('des-ecb', this.key, ''),
			buff
		);
	}

	_process(crypt, buff) {
		// Unfortunately this is not implemented in des.js (browser shim).
		crypt.setAutoPadding(false);

		let a = crypt.update(bufferFrom(buff));

		// Because above is not implemented the following fails in browsers.
		// const b = crypt.final();
		// return this._merge(a, b);

		// Instead just keep adding null bytes until we get 8 bytes out.
		if (a.length < 8) {
			const nullByte = bufferFrom('\0');
			while (a.length < 8) {
				const b = crypt.update(nullByte);
				if (b.length) {
					a = this._merge(a, b);
				}
			}
		}

		// Only return 8 bytes.
		return uint8ArraySlice(a, 0, 8);
	}

	_merge(a, b) {
		const al = a.length;
		const l = al + b.length;
		const r = new Uint8Array(l);
		for (let i = 0; i < l; i++) {
			r[i] = i < al ? a[i] : b[i - al];
		}
		return r;
	}
}
