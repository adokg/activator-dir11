import crypto from 'crypto';
import buffer from 'buffer';

import {AdoSha1} from './adosha1';

const {Buffer} = buffer;

export function codeClean(serial) {
	return serial.replace(/[^0-9]/g, '');
}

export function codeFormat(serial, group) {
	const chars = serial.split('').reverse();
	let r = '';
	for (let i = 0; chars.length; i++) {
		if (i && (i % group) === 0) {
			r += '-';
		}
		r += chars.pop();
	}
	return r;
}

export function random(limit) {
	return Math.floor(Math.random() * limit);
}

export function bufferFrom(data, encoding = null) {
	// Use modern from method if available.
	if (Buffer.from) {
		return encoding ? Buffer.from(data, encoding) : Buffer.from(data);
	}
	return encoding ? new Buffer(data, encoding) : new Buffer(data);
}

export function bnBytes(bn, size = null, offset = 0, pad = false) {
	const bytes = bn.toArray().reverse();
	const a = size ? bytes.slice(offset, offset + size) : bytes.slice(offset);
	if (pad && size) {
		while (a.length < size) {
			a.push(0);
		}
	}
	return new Uint8Array(a);
}

export function bnDig(bn, base) {
	return bn.toString(base).length;
}

export function bnMad(x, y, z, w) {
	let v = x.mul(y);
	if (!x.eq(z) && !y.eq(z)) {
		v = v.add(z)
	}
	return [v.div(w), v.mod(w)];
}

export function idiv(i, d) {
	return Math.floor(i / d);
}

export function xorWithKey(buff, key) {
	for (let i = 0; i < buff.length; i++) {
		buff[i] ^= key[i];
	}
}

export function xorWithByte(buff, byte) {
	for (let i = 0; i < buff.length; i++) {
		buff[i] ^= byte;
	}
}

export function uint8ArrayReversed(a) {
	const l = a.length;
	const le = l - 1;
	const r = new Uint8Array(l);
	for (let i = 0; i < l; i++) {
		r[i] = a[le - i];
	}
	return r;
}

export function uint8ArrayToHex(a, delim = ' ') {
	const r = [];
	for (let i = 0; i < a.length; i++) {
		const v = a[i];
		r.push((v < 0x10 ? '0' : '') + v.toString(16).toUpperCase());
	}
	return r.join(delim);
}

export function uint8ArrayFromHex(str) {
	const matches = str.replace(/\s/g, '').match(/../g);
	const a = matches ? [...matches] : [];
	const r = new Uint8Array(a.length);
	for (let i = 0; i < a.length; i++) {
		r[i] = parseInt(a[i], 16);
	}
	return r;
}

export function uint8ArraySlice(a, offset = 0, size = null) {
	size = size === null ? a.length - offset : size;
	const r = new Uint8Array(size);
	for (let i = 0; i < size; i++) {
		r[i] = a[i + offset];
	}
	return r;
}

export function lpadFixed(s, c, l) {
	let r = s.substr(0, l);
	while (r.length < l) {
		r = c + r;
	}
	return r;
}

export function adoSha1(buff) {
	const ctx = new AdoSha1();
	ctx.input(buff);
	ctx.result();

	// Instead of the resulting hash, the intermediateHash hash is used.
	const bytes = [];
	for (const n of ctx.intermediateHash) {
		bytes.push(n & 0xFF);
		bytes.push((n >> 8) & 0xFF);
		bytes.push((n >> 16) & 0xFF);
		bytes.push((n >> 24) & 0xFF);
	}
	return new Uint8Array(bytes);
}

export function sha1(buff) {
	const shasum = crypto.createHash('sha1');
	shasum.update(buff);
	return new Uint8Array(shasum.digest());
}
