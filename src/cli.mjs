import {Authorization} from './authorization';

export class Cli extends Object {
	constructor(log, error) {
		super();

		this.log = log;
		this.error = error;
	}

	async run(arg0, argv, env) {
		const args = argv.slice(2);
		if (args.length < 1) {
			this.log('args: activation_number [rand_a] [rand_b]');
			return 1;
		}

		const activationNumber = args[0];
		const randA = args.length > 1 ? +args[1] : null;
		const randB = args.length > 2 ? +args[2] : null;

		const auth = new Authorization();
		this.log(auth.code(activationNumber, randA, randB));

		return 0;
	}
}
