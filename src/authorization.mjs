import BN from 'bn.js';
import elliptic from 'elliptic';

import {
	codeClean,
	codeFormat,
	random,
	bnBytes,
	bnDig,
	bnMad,
	idiv,
	xorWithKey,
	xorWithByte,
	uint8ArrayReversed,
	uint8ArrayToHex,
	uint8ArrayFromHex,
	lpadFixed,
	adoSha1,
	sha1
} from './util';
import {
	deskeyA,
	deskeyB,
	xorkey
} from './data';
import {DesEcb} from './desecb';

export class Authorization extends Object {
	constructor() {
		super();

		this.deskeyA = uint8ArrayFromHex(deskeyA);
		this.deskeyB = uint8ArrayFromHex(deskeyB);
		this.xorkey = uint8ArrayFromHex(xorkey);
	}

	code(activationNumber, randA = null, randB = null) {
		randA = (randA & 0x3FFF) || (random(0x3FFF) + 1);
		randB = (randB >>> 0) || (random(0xFFFFFFFF) + 1);

		const activation = codeClean(activationNumber);

		let bigIntA = new BN(activation, 10);
		bigIntA = bigIntA.divn(4);
		bigIntA = bigIntA.divn(8);
		bigIntA = bigIntA.divn(2);
		bigIntA = bigIntA.divn(100);
		bigIntA = bigIntA.divn(2);
		bigIntA = bigIntA.divn(100);

		let bigIntAData = bnBytes(bigIntA, 8, 0, true);
		this.xorWithKey(bigIntAData);

		const desEcbA = this.createDesEcbA();

		const bigIntADataEnc1 = desEcbA.encrypt(bigIntAData);
		const bigIntADataEnc2 = desEcbA.encrypt(bigIntADataEnc1);

		bigIntAData = bnBytes(bigIntA, 8, 0, true);

		const xorc = bigIntADataEnc2[0] ^ bnBytes(bigIntA, 1, 8)[0];

		const bigIntADataDec1 = desEcbA.decrypt(bigIntAData);

		this.xorWithKey(bigIntADataDec1);
		this.xorWithByte(bigIntADataDec1, xorc);

		const desEcbB = this.createDesEcbB();

		const bigIntADataDec2 = desEcbB.decrypt(bigIntADataDec1);
		this.xorWithKey(bigIntADataDec2);

		const bigIntADataDec2Rev = uint8ArrayReversed(bigIntADataDec2);

		bigIntA = new BN(uint8ArrayToHex(bigIntADataDec2Rev, ''), 16);
		bigIntA = bigIntA.divn(0x400000);

		const subd = bigIntA.modn(8);
		bigIntA = bigIntA.divn(8);

		const formatData = this.formatTheInt(bigIntA);

		const bigIntBInt = subd + 8 * randA;
		let bigIntB = new BN(bigIntBInt);
		bigIntB = bigIntB.muln(10000);
		bigIntB = bigIntB.shln(32);
		bigIntB = bigIntB.add(bigIntA);
		bigIntB = bigIntB.muln(10000);
		bigIntB = bigIntB.addn(3051);

		const digd = bnDig(bigIntB, 16);
		const digdHalf = (digd & 2) ? idiv(digd, 2) + 1 : idiv(digd, 2);
		const adosha1Hash = this.adoShaInt(bigIntB, digdHalf);

		bigIntB = new BN(uint8ArrayToHex(uint8ArrayReversed(adosha1Hash), ''), 16);

		let bigIntF = new BN(0x1FFF75);
		let bigIntI = new BN(0xE791D);
		let bigIntJ = new BN(0);
		let bigIntK = new BN(0);
		let bigIntL = new BN(0);

		const ec = new elliptic.curve.short({
			a: new BN(0x1FFFE8),
			b: new BN(0x63B),
			p: new BN(0x1FFFEB)
		});
		const ecPointA = ec.point(new BN(0x1C2DE3), new BN(0x14CC18));
		bigIntJ = new BN(randB);
		if (bigIntJ.eq(bigIntL)) {
			bigIntJ = bigIntJ.addn(15);
		}
		const ecPointB = ecPointA.mul(bigIntJ);

		bigIntK = ecPointB.getX();
		bigIntK = bigIntK.mod(bigIntF);
		bigIntJ = bigIntJ.invm(bigIntF);

		bigIntB = bigIntB.mod(bigIntF);

		bigIntL = bnMad(bigIntI, bigIntK, bigIntB, bigIntF)[1];
		bigIntL = bnMad(bigIntL, bigIntJ, bigIntJ, bigIntF)[1];

		bigIntK = bigIntK.shln(21);
		bigIntK = bigIntK.add(bigIntL);

		bigIntL = new BN(bigIntBInt);
		bigIntL = bigIntL.shln(42);
		bigIntL = bigIntL.add(bigIntK);

		let bigIntLData = uint8ArrayReversed(bnBytes(bigIntL, 8, 0, true));

		const sha1Hash = sha1(formatData);
		this.inlineHashEnc(desEcbB, sha1Hash);

		for (let i = 0; i < 7; i++) {
			bigIntLData[7 - i] ^= sha1Hash[i + 8];
		}
		for (let i = 0; i < 6; i++) {
			bigIntLData[7 - i] ^= bigIntLData[1];
		}
		bigIntLData[0] = (bigIntLData[1] ^ bigIntLData[0]) & 7;

		this.inlineHashEnc(desEcbA, sha1Hash);

		for (let i = 0; i < 7; i++) {
			bigIntLData[7 - i] ^= sha1Hash[i + 8];
		}

		bigIntL = new BN(0);
		for (let i = 0; i < 8; i++) {
			bigIntL = bigIntL.muln(256);
			bigIntL = bigIntL.addn(bigIntLData[i]);
		}

		bigIntL = bigIntL.muln(100);
		bigIntL = bigIntL.addn(30);
		bigIntL = bigIntL.muln(2);
		bigIntL = bigIntL.addn(0);
		bigIntL = bigIntL.muln(100);
		bigIntL = bigIntL.addn(51);
		bigIntL = bigIntL.muln(2);
		bigIntL = bigIntL.addn(1);
		bigIntL = bigIntL.muln(8);
		bigIntL = bigIntL.addn(7);
		bigIntL = bigIntL.muln(4);
		bigIntL = bigIntL.addn(3);

		return codeFormat(lpadFixed(bigIntL.toString(10), '0', 24), 4);
	}

	adoShaInt(bn, size) {
		return adoSha1(bnBytes(bn, size, 0, true));
	}

	formatTheInt(a) {
		const u32bn = new BN(0xFFFFFFFF);
		const sa = a.and(u32bn).toString(16).toUpperCase();
		const sb = a.shrn(32).and(u32bn).toString(10);
		return lpadFixed(sb, '0', 4) + lpadFixed(sa, '0', 8);
	}

	xorWithKey(buff) {
		xorWithKey(buff, this.xorkey);
	}

	xorWithByte(buff, byte) {
		xorWithByte(buff, byte);
	}

	createDesEcbA(buff) {
		return new DesEcb(this.deskeyA);
	}

	createDesEcbB(buff) {
		return new DesEcb(this.deskeyB);
	}

	inlineHashEnc(des, buff) {
		const b = new Uint8Array(8);
		for (let i = 0; i < 8; i++) {
			b[i] = buff[i];
		}
		const o = des.encrypt(b);
		for (let i = 0; i < 8; i++) {
			buff[i + 8] = o[i];
		}
	}
}
