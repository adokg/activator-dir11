#!/usr/bin/env node

'use strict';

(new (require('..').Cli)(
	function(s) {
		console.log(s);
	},
	function(s) {
		console.error(s);
	}
)).run(
	process.argv0,
	process.argv,
	process.env
)
.then(function(code) {
	process.exitCode = code;
})
.catch(function(err) {
	process.exitCode = 1;
	console.error(err);
});
